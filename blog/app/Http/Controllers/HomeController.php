<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Debugbar;
use auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();//manejo de errores
        //dd($user);mostar en pantalla los campos user

        Debugbar::info($user);
        Debugbar::error('Error');
        Debugbar::warning('Watch out_');
        Debugbar::addMessage('Another message', 'mylabel');

        return view('home');
    }
}
